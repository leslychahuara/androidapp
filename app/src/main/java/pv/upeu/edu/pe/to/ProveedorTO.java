package pv.upeu.edu.pe.to;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ProveedorTO {
    int id;
    String NombreProveedor;
    String Ruc;
    String Direccion;
    String Telefono;
    String DatosEncargado;
    String TelefonoEncargado;
    String FechaRegistro;


    public ProveedorTO(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreProveedor() {
        return NombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        NombreProveedor = nombreProveedor;
    }

    public String getRuc() {
        return Ruc;
    }

    public void setRuc(String ruc) {
        Ruc = ruc;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getDatosEncargado() {
        return DatosEncargado;
    }

    public void setDatosEncargado(String datosEncargado) {
        DatosEncargado = datosEncargado;
    }

    public String getTelefonoEncargado() {
        return TelefonoEncargado;
    }

    public void setTelefonoEncargado(String telefonoEncargado) {
        TelefonoEncargado = telefonoEncargado;
    }

    public String getFechaRegistro() {
        return FechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        FechaRegistro = fechaRegistro;
    }

    public static ArrayList<ProveedorTO> obtenerProveedor(String json){
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<ProveedorTO>>(){}.getType();
        return gson.fromJson(json, type);

    }

}
