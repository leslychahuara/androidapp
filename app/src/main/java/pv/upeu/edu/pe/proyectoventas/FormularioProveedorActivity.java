package pv.upeu.edu.pe.proyectoventas;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import pv.upeu.edu.pe.to.ProveedorTO;


public class FormularioProveedorActivity extends AppCompatActivity{
    EditText nombreProveedor,ruc,direccion,telefono,datosEncagado,telefonoEncargado,fechaRegistro;
    ProveedorTO proveedorTO;
    String  id;
    String operacion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_proveedor);

        inicializar();

        Bundle bundle = getIntent().getExtras();
        this.operacion = bundle.getString("operacion");
        if (this.operacion.equals("actualizar")){
            this.id = bundle.getString("id");

            ///

            new ObtenerProveedor().execute();
        }
    }

    public void inicializar() {
        this.nombreProveedor = (EditText) findViewById(R.id.editTextNombreProveedor);
        this.ruc = (EditText) findViewById(R.id.editTextRuc);
        this.direccion = (EditText) findViewById(R.id.editTextDireccion);
        this.telefono = (EditText) findViewById(R.id.editTextTelefono);
        this.datosEncagado = (EditText) findViewById(R.id.editTextDatosEncargado);
        this.telefonoEncargado = (EditText) findViewById(R.id.editTextTelefonoEncargado);
        this.fechaRegistro = (EditText) findViewById(R.id.editTextFechaRegistro);
    }
    public void btnGuardarProveedor(View view){
        proveedorTO = new ProveedorTO();
        proveedorTO.setNombreProveedor(nombreProveedor.getText().toString().trim());
        proveedorTO.setRuc(ruc.getText().toString().trim());
        proveedorTO.setDireccion(direccion.getText().toString().trim());
        proveedorTO.setTelefono(telefono.getText().toString().trim());
        proveedorTO.setDatosEncargado(datosEncagado.getText().toString().trim());
        proveedorTO.setTelefonoEncargado(telefonoEncargado.getText().toString().trim());
        proveedorTO.setFechaRegistro(fechaRegistro.getText().toString().trim());


        if (this.operacion.equals("insertar"))
            new FormularioProveedorActivity.InsertarProveedor().execute();

        if (this.operacion.equals("actualizar"))
            new ActualizarProveedor().execute();

    }

    private class InsertarProveedor extends AsyncTask<Void, Void, Boolean> {
        public Boolean doInBackground(Void... params){
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://172.22.10.51:8089/proveedores/");
            httpPost.setHeader("Content-Type", "application/json");

            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("NombreProveedor", proveedorTO.getNombreProveedor());
                jsonObject.put("Ruc",proveedorTO.getRuc());
                jsonObject.put("Direccion",proveedorTO.getDireccion());
                jsonObject.put("Telefono",proveedorTO.getTelefono());
                jsonObject.put("DatosEncargado",proveedorTO.getDatosEncargado());
                jsonObject.put("TelefonoEncargado",proveedorTO.getTelefonoEncargado());
                jsonObject.put("FechaRegistro",proveedorTO.getFechaRegistro());

                StringEntity stringEntity = new StringEntity(jsonObject.toString());
                httpPost.setEntity(stringEntity);
                httpClient.execute(httpPost);
                return true;

            }catch (org.json.JSONException e){
                return false;
            }catch (java.io.UnsupportedEncodingException e){
                return false;
            }catch (org.apache.http.client.ClientProtocolException e){
                return false;
            }catch (java.io.IOException e){
                return false;
            }

        }

        public void onPostExecute (Boolean result){
            if (result){
                Toast.makeText(FormularioProveedorActivity.this,"Insertado Correctamente", Toast.LENGTH_LONG).show();
            }else
                Toast.makeText(FormularioProveedorActivity.this,"Problema al insertar", Toast.LENGTH_LONG).show();
        }
    }


    private class ActualizarProveedor extends AsyncTask<Void, Void, Boolean>{
        public Boolean doInBackground(Void... params){

            HttpClient httpClient = new DefaultHttpClient();
            HttpPut httpPut = new HttpPut("http://172.22.10.51:8089/proveedores/"+id+"/");
            httpPut.setHeader("Content-Type", "application/json");

            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("NombreProveedor", proveedorTO.getNombreProveedor());
                jsonObject.put("Ruc",proveedorTO.getRuc());
                jsonObject.put("Direccion",proveedorTO.getDireccion());
                jsonObject.put("Telefono",proveedorTO.getTelefono());
                jsonObject.put("DatosEncargado",proveedorTO.getDatosEncargado());
                jsonObject.put("TelefonoEncargado",proveedorTO.getTelefonoEncargado());
                jsonObject.put("FechaRegistro",proveedorTO.getFechaRegistro());

                StringEntity stringEntity = new StringEntity(jsonObject.toString());
                httpPut.setEntity(stringEntity);
                httpClient.execute(httpPut);
                return true;

            }catch (org.json.JSONException e){
                return false;
            }catch (java.io.UnsupportedEncodingException e){
                return false;
            }catch (java.io.IOException e){
                return false;
            }

        }

        public void onPostExecute (Boolean result){
            String msj;
            if (result) {
                msj = "Actualizado Corrextamente";
            }else {
                msj = "Problemas al Actualizar";
            }
            Toast.makeText(FormularioProveedorActivity.this,msj,Toast.LENGTH_LONG).show();

        }


    }

    private class ObtenerProveedor extends AsyncTask<Void,Void,ProveedorTO> {

        public ProveedorTO doInBackground(Void... params) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://172.22.10.51:8089/proveedores/" + id + "/");

            httpGet.setHeader("Content-Type", "application/json");
            proveedorTO = new ProveedorTO();

            try {
                HttpResponse response = httpClient.execute(httpGet);
                String responString = EntityUtils.toString(response.getEntity());

                JSONObject jsonObject = new JSONObject(responString);

                proveedorTO.setNombreProveedor(jsonObject.getString("NombreProveedor"));
                proveedorTO.setRuc(jsonObject.getString("Ruc"));
                proveedorTO.setDireccion(jsonObject.getString("Direccion"));
                proveedorTO.setTelefono(jsonObject.getString("Telefono"));
                proveedorTO.setDatosEncargado(jsonObject.getString("DatosEncargado"));
                proveedorTO.setTelefonoEncargado(jsonObject.getString("TelefonoEncargado"));
                proveedorTO.setFechaRegistro(jsonObject.getString("FechaRegistro"));
                return proveedorTO;

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }

        }

        public void onPostExecute(ProveedorTO proveedorTO){
            super.onPostExecute(proveedorTO);
            if (proveedorTO !=null){
                nombreProveedor.setText(proveedorTO.getNombreProveedor());
                ruc.setText(proveedorTO.getRuc());
                direccion.setText(proveedorTO.getDireccion());
                telefono.setText(proveedorTO.getTelefono());
                datosEncagado.setText(proveedorTO.getDatosEncargado());
                telefonoEncargado.setText(proveedorTO.getTelefonoEncargado());
                fechaRegistro.setText(proveedorTO.getFechaRegistro());

            }else
                Toast.makeText(FormularioProveedorActivity.this,"Problemas",Toast.LENGTH_LONG).show();


        }


    }

}
