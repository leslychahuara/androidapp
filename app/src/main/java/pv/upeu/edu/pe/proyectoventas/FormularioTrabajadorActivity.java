package pv.upeu.edu.pe.proyectoventas;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.IOException;

import pv.upeu.edu.pe.to.TrabajadorTO;

public class FormularioTrabajadorActivity extends AppCompatActivity {

    EditText nombre,apellidoPaterno,apellidoMaterno,dni,correo,telefono,cargo,fechaIngreso;
    TrabajadorTO trabajador;
    String id;

    //
    String operacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_trabajador);

        inicializar();

        Bundle bundle = getIntent().getExtras();
        this.operacion = bundle.getString("operacion");
        if (this.operacion.equals("actualizar")){
            this.id = bundle.getString("id_trabajador");
        }

    }

    public void inicializar(){
        this.nombre = (EditText) findViewById(R.id.editTextNombre);
        this.apellidoPaterno = (EditText) findViewById(R.id.editTextApPaterno);
        this.apellidoMaterno = (EditText) findViewById(R.id.editTextApMaterno);
        this.dni = (EditText) findViewById(R.id.editTextDNI);
        this.correo = (EditText) findViewById(R.id.editTextCorreo);
        this.telefono = (EditText) findViewById(R.id.editTextTelefono);
        this.cargo = (EditText) findViewById(R.id.editTextCargo);
        this.fechaIngreso = (EditText) findViewById(R.id.editTextFechaIngreso);

    }

    public void btnGuardarTrabajador(View view){
        trabajador = new TrabajadorTO();
        trabajador.setNombre(nombre.getText().toString().trim());
        trabajador.setApellidoPaterno(apellidoPaterno.getText().toString().trim());
        trabajador.setApellidoMaterno(apellidoMaterno.getText().toString().trim());
        trabajador.setDNI(dni.getText().toString().trim());
        trabajador.setCorreo(correo.getText().toString().trim());
        trabajador.setTelefono(telefono.getText().toString().trim());
        trabajador.setCargo(cargo.getText().toString().trim());
        trabajador.setFechaIngreso(fechaIngreso.getText().toString().trim());


        if (this.operacion.equals("insertar"))
            new InsertarTrabajador().execute();
        if (this.operacion.equals("actualizar"))
            new ActualizarTrabajador().execute();

    }

    //insertar trabajador
    private class InsertarTrabajador extends AsyncTask<Void, Void, Boolean>{
        public Boolean doInBackground(Void... params){
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://172.22.10.51:8089/trabajadores/");
            httpPost.setHeader("Content-Type", "application/json");

            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("Nombre", trabajador.getNombre());
                jsonObject.put("ApellidoPaterno",trabajador.getApellidoPaterno());
                jsonObject.put("ApellidoMaterno",trabajador.getApellidoMaterno());
                jsonObject.put("Correo",trabajador.getCorreo());
                jsonObject.put("DNI",trabajador.getDNI());
                jsonObject.put("Telefono",trabajador.getTelefono());
                jsonObject.put("Cargo",trabajador.getCargo());
                jsonObject.put("FechaIngreso",trabajador.getFechaIngreso());

                StringEntity stringEntity = new StringEntity(jsonObject.toString());
                httpPost.setEntity(stringEntity);
                httpClient.execute(httpPost);
                return true;

            }catch (org.json.JSONException e){
                return false;
            }catch (java.io.UnsupportedEncodingException e){
                return false;
            }catch (org.apache.http.client.ClientProtocolException e){
                return false;
            }catch (java.io.IOException e){
                return false;
            }

        }

        public void onPostExecute (Boolean result){
            if (result){
                Toast.makeText(FormularioTrabajadorActivity.this,"Insertado Correctamente", Toast.LENGTH_LONG).show();
            }else
                Toast.makeText(FormularioTrabajadorActivity.this,"Problema al insertar", Toast.LENGTH_LONG).show();
        }
    }

    private class ActualizarTrabajador extends AsyncTask<Void, Void, Boolean>{
        public Boolean doInBackground(Void... params){

            HttpClient httpClient = new DefaultHttpClient();
            HttpPut httpPut = new HttpPut("http://172.22.10.51:8089/trabajadores/"+id+"/");
            httpPut.setHeader("Content-Type", "application/json");

            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("Nombre", trabajador.getNombre());
                jsonObject.put("ApellidoPaterno",trabajador.getApellidoPaterno());
                jsonObject.put("ApellidoMaterno",trabajador.getApellidoMaterno());
                jsonObject.put("Correo",trabajador.getCorreo());
                jsonObject.put("DNI",trabajador.getDNI());
                jsonObject.put("Telefono",trabajador.getTelefono());
                jsonObject.put("Cargo",trabajador.getCargo());
                jsonObject.put("FechaIngreso",trabajador.getFechaIngreso());

                StringEntity stringEntity = new StringEntity(jsonObject.toString());
                httpPut.setEntity(stringEntity);
                httpClient.execute(httpPut);
                return true;

            }catch (org.json.JSONException e){
                return false;
            }catch (java.io.UnsupportedEncodingException e){
                return false;
            }catch (java.io.IOException e){
                return false;
            }

        }

        public void onPostExecute (Boolean result){
            String msj;
            if (result) {
                msj = "Actualizado Correctamente";
            }else {
                msj = "Problemas al Actualizar";
            }
            Toast.makeText(FormularioTrabajadorActivity.this,msj,Toast.LENGTH_LONG).show();

        }


        }

    }
