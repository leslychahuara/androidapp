package pv.upeu.edu.pe.proyectoventas;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import pv.upeu.edu.pe.to.HttpRequest;
import pv.upeu.edu.pe.to.ProveedorTO;
import pv.upeu.edu.pe.util.ProveedorAdapter;

public class BuscarProveedorActivity extends AppCompatActivity {
    Spinner spinnerParametroProveedor;
    EditText dato;
    ListView listViewProveedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_proveedor);
        inicializar();

    }


    public void inicializar(){
        this.spinnerParametroProveedor = (Spinner)findViewById(R.id.spinnerProveedorParametros);
        this.dato = (EditText)findViewById(R.id.editTextDato);
        this.listViewProveedor = (ListView)findViewById(R.id.listProveedor);

        new getProveedor().execute("http://172.22.10.51:8089/proveedores/");
    }

    //get trabajador
    private class getProveedor extends AsyncTask<String, Void, String>{
        public String doInBackground(String... params){
            try {
                return HttpRequest.get(params[0]).accept("application/json").body();
            }catch (Exception e){
                return "";
            }
        }
        public void  onPostExecute(String result){
            if (result.isEmpty()){
                Toast.makeText(BuscarProveedorActivity.this,"No se generan resultados",Toast.LENGTH_LONG).show();
            }else {
                ArrayList<ProveedorTO> proveedores = ProveedorTO.obtenerProveedor(result);
                ArrayList<ProveedorTO> proveedores_aux = new ArrayList<>();

                if (spinnerParametroProveedor.getSelectedItem().toString().equals("Listar Todo")){
                    proveedores_aux = proveedores;
                }else {
                    for (int i = 0; i < proveedores.size(); i++) {
                        switch (spinnerParametroProveedor.getSelectedItem().toString()){
                            case "Nombre Proveedor":
                                if (proveedores.get(i).getNombreProveedor().equals(dato.getText().toString().trim())){
                                    proveedores_aux.add(proveedores.get(i));
                                }
                                break;
                            case "Ruc":
                                if (proveedores.get(i).getRuc().equals(dato.getText().toString().trim())){
                                    proveedores_aux.add(proveedores.get(i));
                                }
                                break;
                            case "DNI":
                                if (proveedores.get(i).getDireccion().equals(dato.getText().toString().trim())){
                                    proveedores_aux.add(proveedores.get(i));
                                }
                                break;
                        }
                        
                    }
                }
                if (proveedores_aux.size() !=0){
                    ProveedorAdapter adapter = new ProveedorAdapter(BuscarProveedorActivity.this,proveedores_aux);
                    listViewProveedor.setAdapter(adapter);
                    listViewProveedor.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                        @Override

                        public void onItemClick(AdapterView<?> parent, View view,int position,long id){
                            Intent i = new Intent(BuscarProveedorActivity.this, FormularioProveedorActivity.class);
                            i.putExtra("operacion","actualizar");
                            i.putExtra("id",((ProveedorTO)parent.getAdapter().getItem(position)).getNombreProveedor());
                            startActivity(i);
                        }

                    });
                }
            }
        }
    }

    public void btnFormularioProveedor(View view){
        Intent intent = new Intent(BuscarProveedorActivity.this,FormularioProveedorActivity.class);
        intent.putExtra("operacion","insertar");
        startActivity(intent);
    }
}
