package pv.upeu.edu.pe.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pv.upeu.edu.pe.proyectoventas.R;
import pv.upeu.edu.pe.to.TrabajadorTO;

public class TrabajadorAdapter extends BaseAdapter {
    Context context;
    ArrayList<TrabajadorTO> trabajadorTOArrayList;

    public TrabajadorAdapter(Context context, ArrayList<TrabajadorTO> trabajadorTOArrayList) {
        this.context = context;
        this.trabajadorTOArrayList = trabajadorTOArrayList;
    }

    @Override
    public int getCount() {
        return this.trabajadorTOArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.trabajadorTOArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //creacion de vista
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view  = layoutInflater.inflate(R.layout.lista_trabajador, parent, false);
        //objeto de formulario
        TextView nombre = (TextView) view.findViewById(R.id.textViewNombre);
        TextView apellidoPaterno = (TextView) view.findViewById(R.id.textViewApellidoPaterno);
        TextView dni = (TextView) view.findViewById(R.id.textViewDNI);

        TrabajadorTO trabajador = this.trabajadorTOArrayList.get(position);
        if (trabajador != null){
            nombre.setText("Nombre: "+trabajador.getNombre());
            apellidoPaterno.setText("Apellido Paterno: "+trabajador.getApellidoPaterno());
            dni.setText("DNI: "+trabajador.getDNI());
        }

        return view;
    }
}
