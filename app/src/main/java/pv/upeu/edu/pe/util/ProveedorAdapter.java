package pv.upeu.edu.pe.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pv.upeu.edu.pe.proyectoventas.R;
import pv.upeu.edu.pe.to.ProveedorTO;

public class ProveedorAdapter extends BaseAdapter {
    Context context;
    ArrayList<ProveedorTO> proveedorTOArrayList;

    public ProveedorAdapter(Context context, ArrayList<ProveedorTO> proveedorTOArrayList) {
        this.context = context;
        this.proveedorTOArrayList = proveedorTOArrayList;
    }

    @Override
    public int getCount() {
        return this.proveedorTOArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.proveedorTOArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //creacion de vista
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view  = layoutInflater.inflate(R.layout.lista_proveedor, parent, false);
        //objeto de formulario
        TextView nombreProveedor = (TextView) view.findViewById(R.id.textViewNombreProveedor);
        TextView ruc = (TextView) view.findViewById(R.id.textViewRuc);
        TextView direccion = (TextView) view.findViewById(R.id.textViewDireccion);

        ProveedorTO proveedor = this.proveedorTOArrayList.get(position);
        if (proveedor != null){
            nombreProveedor.setText("Nombre: "+proveedor.getNombreProveedor());
            ruc.setText("Apellido Paterno: "+proveedor.getRuc());
            direccion.setText("DNI: "+proveedor.getDireccion());
        }

        return view;
    }
}
