package pv.upeu.edu.pe.proyectoventas;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import pv.upeu.edu.pe.to.HttpRequest;
import pv.upeu.edu.pe.to.TrabajadorTO;
import pv.upeu.edu.pe.util.TrabajadorAdapter;

public class BuscarProductoActivity extends AppCompatActivity {
    Spinner spinnerParametro;
    EditText dato;
    ListView listViewTrabajador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_trabajador);
        inicializar();

    }


    public void inicializar(){
        this.spinnerParametro = (Spinner)findViewById(R.id.spinnerTrabajadorParametros);
        this.dato = (EditText)findViewById(R.id.editTextDato);
        this.listViewTrabajador = (ListView)findViewById(R.id.listTrabajador);

        new getTrabajador().execute("http://172.22.10.51:7000/Trabajadores/");
    }

    //get trabajador
    private class getTrabajador extends AsyncTask<String, Void, String>{
        public String doInBackground(String... params){
            try {
                return HttpRequest.get(params[0]).accept("application/json").body();
            }catch (Exception e){
                return "";
            }
        }
        public void  onPostExecute(String result){
            if (result.isEmpty()){
                Toast.makeText(BuscarProductoActivity.this,"No se generan resultados",Toast.LENGTH_LONG).show();
            }else {
                ArrayList<TrabajadorTO> trabajadores = TrabajadorTO.obtenerTrabajador(result);
                ArrayList<TrabajadorTO> trabajadores_aux = new ArrayList<>();

                if (spinnerParametro.getSelectedItem().toString().equals("Listar Todo")){
                    trabajadores_aux = trabajadores;
                }else {
                    for (int i = 0; i < trabajadores.size(); i++) {
                        switch (spinnerParametro.getSelectedItem().toString()){
                            case "Nombre":
                                if (trabajadores.get(i).getNombre().equals(dato.getText().toString().trim())){
                                    trabajadores_aux.add(trabajadores.get(i));
                                }
                                break;
                            case "Apellido Paterno":
                                if (trabajadores.get(i).getApellidoPaterno().equals(dato.getText().toString().trim())){
                                    trabajadores_aux.add(trabajadores.get(i));
                                }
                                break;
                            case "DNI":
                                if (trabajadores.get(i).getDNI().equals(dato.getText().toString().trim())){
                                    trabajadores_aux.add(trabajadores.get(i));
                                }
                                break;
                        }
                        
                    }
                }
                if (trabajadores_aux.size() !=0){
                    TrabajadorAdapter adapter = new TrabajadorAdapter(BuscarProductoActivity.this,trabajadores_aux);
                    listViewTrabajador.setAdapter(adapter);
                }
            }
        }
    }
}
