package pv.upeu.edu.pe.to;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class UsuarioTO {
    int id;
    String Nombre;
    String ApellidoPaterno;
    String ApellidoMaterno;
    String Correo;
    String DNI;
    String Telefono;
    String Cargo;
    String FechaIngreso;
    String Estado;


    public UsuarioTO(){

    }


    public static ArrayList<UsuarioTO> obtenerTrabajador(String json){
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<UsuarioTO>>(){}.getType();
        return gson.fromJson(json, type);

    }

}
